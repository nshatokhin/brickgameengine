#include "renderer.h"
#include "ui_renderer.h"

#include <QDebug>
#include <QPainter>

#include "engine/commands/commandexit.h"

#include "games/snake/gamesnake.h"

Renderer::Renderer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Renderer),
    m_currentGame(nullptr),
    m_pixelActive("://images/pixel_active.png"),
    m_pixelNotActive("://images/pixel_not_active.png"),
    m_background("://images/background.png"),
    m_observer(nullptr)
{
    ui->setupUi(this);

    m_observer = new ObserverBase(this);

    m_baseCommandMapping.buttonExit = new CommandExit();

    m_currentGame = new GameSnake(m_baseCommandMapping, m_observer);

    m_updateTimer.setInterval(500);
    m_updateTimer.start();
    connect(&m_updateTimer, &QTimer::timeout, this, &Renderer::timerTriggered);
}

Renderer::~Renderer()
{
    delete m_baseCommandMapping.buttonExit;
    delete m_observer;
    delete ui;
}

void Renderer::reset()
{
    delete m_currentGame;
    m_currentGame = new GameSnake(m_baseCommandMapping, m_observer);
}

void Renderer::gameOver()
{
    qDebug() << "Game over!";
    reset();
}

void Renderer::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.drawImage(0, 0, m_background);

    int32_t fieldWidth = m_currentGame->gameFieldWidth();
    int32_t fieldHeigth = m_currentGame->gameFieldHeight();
    GameField gameField = m_currentGame->gameField();

    int32_t pixelWidth = m_pixelActive.width();
    int32_t pixelHeight = m_pixelActive.height();

    for (int32_t i = 0; i < fieldWidth; i++)
    {
        for(int32_t j = 0; j < fieldHeigth; j++)
        {
            if(gameField[i][j] == 1) {
                painter.drawImage(SCREEN_POS_X + pixelWidth * i, SCREEN_POS_Y + pixelHeight * j, m_pixelActive);
            } else {
                painter.drawImage(SCREEN_POS_X + pixelWidth * i, SCREEN_POS_Y + pixelHeight * j, m_pixelNotActive);
            }
        }
    }

}

void Renderer::keyPressEvent(QKeyEvent *event)
{
    Commands * controls = m_currentGame->controls();

    if( event->key() == KEY_EXIT ) {
        m_currentGame->processCommand(controls->buttonExit);
    }
    else if( event->key() == KEY_UP ) {
        m_currentGame->processCommand(controls->buttonUp);
    }
    else if( event->key() == KEY_DOWN ) {
        m_currentGame->processCommand(controls->buttonDown);
    }
    else if( event->key() == KEY_LEFT ) {
        m_currentGame->processCommand(controls->buttonLeft);
    }
    else if( event->key() == KEY_RIGHT ) {
        m_currentGame->processCommand(controls->buttonRight);
    }
    else if( event->key() == KEY_ACTION ) {
        m_currentGame->processCommand(controls->buttonAction);
    }

    repaint();
}

void Renderer::timerTriggered()
{
    m_currentGame->update();

    repaint();
}

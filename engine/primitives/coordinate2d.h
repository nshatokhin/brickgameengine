#ifndef COORDINATE2D_H
#define COORDINATE2D_H

#include <cstdint>

class Coordinate2D
{
public:
        int32_t x, y;

public:
        Coordinate2D(int32_t xv, int32_t yv) : x(xv), y(yv) {}
};

#endif // COORDINATE2D_H

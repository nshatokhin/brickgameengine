#include "observerbase.h"

ObserverBase::ObserverBase(IObservable *subject) :
    m_subject(subject)
{

}

void ObserverBase::reset()
{
    m_subject->reset();
}

void ObserverBase::gameOver()
{
    m_subject->gameOver();
}

#include "gamebase.h"

#include "commandbase.h"

#include "commands/commandaction.h"
#include "commands/commandup.h"
#include "commands/commanddown.h"
#include "commands/commandleft.h"
#include "commands/commandright.h"

GameBase::GameBase(Commands commandsMapping, ObserverBase *observer, int32_t fieldWidth, int32_t fieldHeight) :
    m_commandMap(commandsMapping),
    m_observer(observer),
    m_gameFieldWidth(fieldWidth),
    m_gameFieldHeight(fieldHeight),
    m_gameField(nullptr)
{
    m_commandMap.buttonUp = new CommandUp();
    m_commandMap.buttonDown = new CommandDown();
    m_commandMap.buttonLeft = new CommandLeft();
    m_commandMap.buttonRight = new CommandRight();
    m_commandMap.buttonAction = new CommandAction();

    m_gameField = new uint8_t*[m_gameFieldWidth];
    for(int32_t i = 0; i < m_gameFieldWidth; i++)
    {
        m_gameField[i] = new uint8_t[m_gameFieldHeight];

        for(int32_t j = 0; j < m_gameFieldHeight; j++)
        {
            m_gameField[i][j] = 0;
        }
    }

    rowCounter = 0;
    colCounter = 0;
}

GameBase::~GameBase()
{
    delete m_commandMap.buttonUp;
    delete m_commandMap.buttonDown;
    delete m_commandMap.buttonLeft;
    delete m_commandMap.buttonRight;
    delete m_commandMap.buttonAction;

    for(int32_t i = 0; i < m_gameFieldWidth; i++)
    {
        delete[] m_gameField[i];
    }

    delete[] m_gameField;
}

void GameBase::clearField()
{
    for(int32_t i = 0; i < m_gameFieldWidth; i++)
    {
        for(int32_t j = 0; j < m_gameFieldHeight; j++)
        {
            m_gameField[i][j] = 0;
        }
    }
}

//void GameBase::update()
//{
//    m_gameField[rowCounter][colCounter] = !m_gameField[rowCounter][colCounter];
//    colCounter++;

//    if(colCounter >= m_gameFieldWidth) {
//        colCounter = 0;
//        rowCounter++;
//    }

//    if(rowCounter >= m_gameFieldHeight) {
//        rowCounter = 0;
//        colCounter = 0;
//    }
//}

void GameBase::processCommand(CommandBase *command)
{
    if(command) {
        command->execute(this);
    }
}

GameField GameBase::gameField() const
{
    return m_gameField;
}

int32_t GameBase::gameFieldWidth() const
{
    return m_gameFieldWidth;
}

int32_t GameBase::gameFieldHeight() const
{
    return m_gameFieldHeight;
}

ObserverBase *GameBase::observer()
{
    return m_observer;
}

Commands * GameBase::controls()
{
    return &m_commandMap;
}

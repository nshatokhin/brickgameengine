#ifndef OBSERVERBASE_H
#define OBSERVERBASE_H

#include "iobservable.h"

class ObserverBase
{
public:
    ObserverBase(IObservable *subject);

    void reset();
    void gameOver();

protected:
    IObservable * m_subject;
};

#endif // OBSERVERBASE_H

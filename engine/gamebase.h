#ifndef GAMEBASE_H
#define GAMEBASE_H

#include <cstdint>

class CommandBase;
class ObserverBase;

typedef uint8_t ** GameField;

struct Commands
{
    CommandBase * buttonUp;
    CommandBase * buttonDown;
    CommandBase * buttonLeft;
    CommandBase * buttonRight;
    CommandBase * buttonAction;
    CommandBase * buttonStartPause;
    CommandBase * buttonSound;
    CommandBase * buttonSettings;
    CommandBase * buttonExit;
};

class GameBase
{
public:
    GameBase(Commands commandsMapping, ObserverBase *observer, int32_t fieldWidth = 10, int32_t fieldHeight = 20);
    virtual ~GameBase();

    virtual void update() = 0;
    void clearField();
    void processCommand(CommandBase *command);

    GameField gameField() const;
    int32_t gameFieldWidth() const;
    int32_t gameFieldHeight() const;

    ObserverBase *observer();
    Commands *controls();

    virtual void commandUp() {}
    virtual void commandDown() {}
    virtual void commandLeft() {}
    virtual void commandRight() {}
    virtual void commandAction() {}

protected:
    Commands m_commandMap;
    ObserverBase *m_observer;
    int32_t m_gameFieldWidth, m_gameFieldHeight;
    GameField m_gameField;

    int32_t rowCounter, colCounter;
};

#endif // GAMEBASE_H

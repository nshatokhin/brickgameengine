#ifndef COMMANDDOWN_H
#define COMMANDDOWN_H

#include "engine/commandbase.h"
#include "engine/gamebase.h"

class CommandDown : public CommandBase
{
public:
    CommandDown() : CommandBase() {}
    virtual ~CommandDown() {}

    virtual void execute(GameBase * game) {
        game->commandDown();
    }
};

#endif // COMMANDDOWN_H

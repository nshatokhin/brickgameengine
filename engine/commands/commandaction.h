#ifndef COMMANDACTION_H
#define COMMANDACTION_H

#include "engine/commandbase.h"
#include "engine/gamebase.h"

class CommandAction : public CommandBase
{
public:
    CommandAction() : CommandBase() {}
    virtual ~CommandAction() {}

    virtual void execute(GameBase * game) {
        game->commandAction();
    }
};

#endif // COMMANDACTION_H

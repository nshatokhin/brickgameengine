#ifndef COMMANDLEFT_H
#define COMMANDLEFT_H

#include "engine/commandbase.h"
#include "engine/gamebase.h"

class CommandLeft : public CommandBase
{
public:
    CommandLeft() : CommandBase() {}
    virtual ~CommandLeft() {}

    virtual void execute(GameBase * game) {
        game->commandLeft();
    }
};

#endif // COMMANDLEFT_H

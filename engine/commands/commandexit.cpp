#include "commandexit.h"

#include "engine/gamebase.h"
#include "engine/observerbase.h"

CommandExit::CommandExit()
{

}

void CommandExit::execute(GameBase *game)
{
    if(game->observer()) {
        game->observer()->reset();
    }
}

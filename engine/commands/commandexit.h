#ifndef COMMANDEXIT_H
#define COMMANDEXIT_H

#include "engine/commandbase.h"

class CommandExit : public CommandBase
{
public:
    CommandExit();

    virtual void execute(GameBase * game);
};

#endif // COMMANDEXIT_H

#ifndef COMMANDRIGHT_H
#define COMMANDRIGHT_H

#include "engine/commandbase.h"
#include "engine/gamebase.h"

class CommandRight : public CommandBase
{
public:
    CommandRight() : CommandBase() {}
    virtual ~CommandRight() {}

    virtual void execute(GameBase * game) {
        game->commandRight();
    }
};

#endif // COMMANDRIGHT_H

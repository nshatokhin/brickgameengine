#ifndef COMMANDUP_H
#define COMMANDUP_H

#include "engine/commandbase.h"
#include "engine/gamebase.h"

class CommandUp : public CommandBase
{
public:
    CommandUp() : CommandBase() {}
    virtual ~CommandUp() {}

    virtual void execute(GameBase * game) {
        game->commandUp();
    }
};

#endif // COMMANDUP_H

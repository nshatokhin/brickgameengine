#ifndef IOBSERVABLE_H
#define IOBSERVABLE_H


class IObservable
{
public:
    IObservable() {}
    virtual ~IObservable() {}

    virtual void reset() = 0;
    virtual void gameOver() = 0;
};

#endif // IOBSERVABLE_H

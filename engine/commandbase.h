#ifndef COMMANDBASE_H
#define COMMANDBASE_H

class GameBase;

class CommandBase
{
public:
    CommandBase() {}
    virtual ~CommandBase() {}

    virtual void execute(GameBase * game) = 0;
};

#endif // COMMANDBASE_H

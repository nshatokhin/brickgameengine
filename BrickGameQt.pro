#-------------------------------------------------
#
# Project created by QtCreator 2019-08-02T00:31:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BrickGameQt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        engine/commands/commandexit.cpp \
        engine/gamebase.cpp \
        engine/iobservable.cpp \
        engine/observerbase.cpp \
        games/snake/gamesnake.cpp \
        main.cpp \
        renderer.cpp

HEADERS += \
        engine/commandbase.h \
        engine/commands/commandaction.h \
        engine/commands/commanddown.h \
        engine/commands/commandexit.h \
        engine/commands/commandleft.h \
        engine/commands/commandright.h \
        engine/commands/commandup.h \
        engine/gamebase.h \
        engine/iobservable.h \
        engine/observerbase.h \
        engine/primitives/coordinate2d.h \
        games/snake/gamesnake.h \
        renderer.h

FORMS += \
        renderer.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    gameresourses.qrc

DISTFILES += \
    images/background.png \
    images/pixel_active.png \
    images/pixel_not_active.png

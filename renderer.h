#ifndef RENDERER_H
#define RENDERER_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QTimer>

#include "engine/observerbase.h"
#include "engine/gamebase.h"
#include "engine/iobservable.h"

#define KEY_EXIT Qt::Key_Escape
#define KEY_UP Qt::Key_Up
#define KEY_DOWN Qt::Key_Down
#define KEY_LEFT Qt::Key_Left
#define KEY_RIGHT Qt::Key_Right
#define KEY_ACTION Qt::Key_Space

namespace Ui {
class Renderer;
}

class Renderer : public QMainWindow, public IObservable
{
    Q_OBJECT

public:
    explicit Renderer(QWidget *parent = nullptr);
    virtual ~Renderer();

    virtual void reset();
    virtual void gameOver();

private:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    static constexpr int32_t SCREEN_POS_X = 82;
    static constexpr int32_t SCREEN_POS_Y = 59;

    Ui::Renderer *ui;
    QTimer m_updateTimer;
    GameBase * m_currentGame;

    QImage m_pixelActive, m_pixelNotActive, m_background;
    Commands m_baseCommandMapping;
    ObserverBase *m_observer;

private slots:
    void timerTriggered();
};

#endif // RENDERER_H

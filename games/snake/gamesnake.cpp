#include "gamesnake.h"

#include <stdlib.h>
#include <time.h>

#include "engine/observerbase.h"

GameSnake::GameSnake(Commands commandsMapping, ObserverBase *observer) : GameBase (commandsMapping, observer),
    m_drawTarget(true),
    m_startPos(m_gameFieldWidth / 2, m_gameFieldHeight / 2),
    m_targetPos(0, 0),
    m_direction(0, -1)
{
    srand (static_cast<unsigned int>(time(nullptr)));

    for (int i = 0; i < SNAKE_START_LENGTH; i++) {
        m_snake.push_back(Coordinate2D(m_startPos.x, m_startPos.y + i));
    }

    putRandomTarget();
}

void GameSnake::update()
{
    Coordinate2D head = m_snake.front();
    head.x += m_direction.x;
    head.y += m_direction.y;

    if(head.x < 0 || head.x >= m_gameFieldWidth ||
       head.y < 0 || head.y >= m_gameFieldHeight)
    {
        gameOver();
        return;
    }

    for (auto i = m_snake.begin(); i != m_snake.end(); i++) {
        Coordinate2D snakeElement = *i;

        if(head.x == snakeElement.x && head.y == snakeElement.y) {
            gameOver();
            return;
        }
    }

    m_snake.push_front(head);

    if(head.x == m_targetPos.x && head.y == m_targetPos.y) {
        putRandomTarget();
    } else {
        m_snake.pop_back();
    }

    commit();

    m_drawTarget = !m_drawTarget;
}

void GameSnake::commandUp()
{
    if(m_direction.x == 0 && m_direction.y == 1)
        return;

    m_direction.x = 0;
    m_direction.y = -1;
}

void GameSnake::commandDown()
{
    if(m_direction.x == 0 && m_direction.y == -1)
        return;

    m_direction.x = 0;
    m_direction.y = 1;
}

void GameSnake::commandLeft()
{
    if(m_direction.x == 1 && m_direction.y == 0)
        return;

    m_direction.x = -1;
    m_direction.y = 0;
}

void GameSnake::commandRight()
{
    if(m_direction.x == -1 && m_direction.y == 0)
        return;

    m_direction.x = 1;
    m_direction.y = 0;
}

void GameSnake::putRandomTarget()
{
    m_targetPos.x = rand() % m_gameFieldWidth;
    m_targetPos.y = rand() % m_gameFieldHeight;
}

void GameSnake::commit()
{
    clearField();

    for (auto i = m_snake.begin(); i != m_snake.end(); i++) {
        Coordinate2D snakeElement = *i;
        m_gameField[snakeElement.x][snakeElement.y] = 1;
    }

    if(m_drawTarget) {
        m_gameField[m_targetPos.x][m_targetPos.y] = 1;
    } else {
        m_gameField[m_targetPos.x][m_targetPos.y] = 0;
    }
}

void GameSnake::gameOver()
{
    if(m_observer) {
        m_observer->gameOver();
    }
}

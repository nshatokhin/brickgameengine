#ifndef GAMESNAKE_H
#define GAMESNAKE_H

#include "engine/gamebase.h"
#include "engine/primitives/coordinate2d.h"

#include <list>

class GameSnake : public GameBase
{
public:
    GameSnake(Commands commandsMapping, ObserverBase *observer);

    virtual void update();

    virtual void commandUp();
    virtual void commandDown();
    virtual void commandLeft();
    virtual void commandRight();

protected:
    void putRandomTarget();

    void commit();
    void gameOver();

protected:
    static constexpr int SNAKE_START_LENGTH = 3;

    bool m_drawTarget;

    Coordinate2D m_startPos;

    Coordinate2D m_targetPos;
    Coordinate2D m_direction;
    std::list<Coordinate2D> m_snake;
};

#endif // GAMESNAKE_H
